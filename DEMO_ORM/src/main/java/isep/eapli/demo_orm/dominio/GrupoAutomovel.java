/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Wimy Carvalho
 */
@Entity
public class GrupoAutomovel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idGrupoAutomovel;
    private String nome;
    private int portas;
    private String classe;

    // Para efeitos de ORM 
    protected GrupoAutomovel() {
    }

    public GrupoAutomovel(String nome, int portas, String classe) {
        this.nome = nome;
        this.portas = portas;
        this.classe = classe;
    }

    public void setNportas(int portas) {
        this.portas = portas;
    }

    public int getNportas() {
        return this.portas;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (int) (this.idGrupoAutomovel ^ (this.idGrupoAutomovel >>> 32));
        hash = 37 * hash + Objects.hashCode(this.nome);
        hash = 37 * hash + this.portas;
        hash = 37 * hash + Objects.hashCode(this.classe);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoAutomovel other = (GrupoAutomovel) obj;
        return true;
    }

    @Override
    public String toString() {
        return "\nID_GrupoAutomovel: " + idGrupoAutomovel + "\nnome = " + nome + "\nPortas: " + portas + "\nClasse:" + classe + "\n";
    }

}
