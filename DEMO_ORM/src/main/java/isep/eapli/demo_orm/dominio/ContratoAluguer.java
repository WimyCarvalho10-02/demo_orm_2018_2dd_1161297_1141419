/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Eduardo Gomes
 */
@Entity
public class ContratoAluguer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long IdContratoAluguer;
    @ManyToOne(cascade = CascadeType.ALL)
    private Cliente Cliente;
    private Automovel Automovel;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar DataIni;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar DataFim;
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Condutor> ListaCondutores;

    protected ContratoAluguer() {
    }

    public ContratoAluguer(Cliente Cliente, Automovel Automovel, Calendar DataIni, Calendar DataFim) {
        this.Cliente = Cliente;
        this.Automovel = Automovel;
        this.DataIni =  DataIni;
        this.DataFim = DataFim;
        this.ListaCondutores = new ArrayList<>();
    }
    
    public void addCondutorAutorizado(Condutor c){
        this.ListaCondutores.add(c);
    }
    @Override
    public String toString() {
        return "*** ContratoAluguer ***\n" + "IdContratoAluguer : " + IdContratoAluguer 
                + "\nCliente : " + Cliente + ", Automovel : " + Automovel 
                + "\nDataIni : " + DataIni.getTime() + "\nDataFim : " + DataFim.getTime() +"\n";
    }

}
