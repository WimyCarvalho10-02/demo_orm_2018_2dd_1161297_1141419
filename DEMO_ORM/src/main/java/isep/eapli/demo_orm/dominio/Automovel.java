/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Eduardo Gomes
 */
@Entity
public class Automovel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long IDGrupoAutomovel;
    private int km;
    private String matricula;
    private GrupoAutomovel Grupo;

    protected Automovel() {
    }

    public Automovel(int km, String matricula, GrupoAutomovel Grupo) {
        this.km = km;
        this.matricula = matricula;
        this.Grupo = Grupo;
    }

    public void alterarPortas(int nPortas) {
        this.Grupo.setNportas(nPortas);
    }

    public GrupoAutomovel getGrupo() {
        return this.Grupo;
    }

    public void setGrupo(GrupoAutomovel Grupo) {
        this.Grupo = Grupo;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public String toString() {
        return "Automovel -> {ID:" + IDGrupoAutomovel + "km = " + km + ", matricula= " + matricula
                + ", Grupo= " + Grupo.toString() + '}';
    }

}
