/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author wimyd
 */
@Entity
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long IDPagamento;
    private TipoPagamento tipo;
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="IdContratoAluguer")
    private ContratoAluguer contrato;
    

    //para efeitos ORM
    protected Pagamento(){
        
    }
    
    public Pagamento(TipoPagamento tipo, ContratoAluguer contrato) {
        this.tipo = tipo;
        this.contrato = contrato;
    }

    public TipoPagamento getTipo() {
        return tipo;
    }

    public void setTipo(TipoPagamento tipo) {
        this.tipo = tipo;
    }

    public ContratoAluguer getContrato() {
        return contrato;
    }

    @Override
    public String toString() {
        return "**** Pagamento ****\n" + "IDPagamento : " + IDPagamento + "\nTipoPagamento :" + tipo.toString() + "\nContrato : " + contrato + "\n";
    }
    
    
}
