/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author WImy Carvalho
 */
@Entity
public class Condutor implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long IDcondutor;
    private String nome;
    private String NumCartaConducao;

    protected Condutor() {

    }

    public Condutor(String nome, String NumCartaConducao) {
        this.nome = nome;
        this.NumCartaConducao = NumCartaConducao;
    }

    @Override
    public String toString() {
        return "---- Condutor -----\n"
                + "IDcondutor : " + IDcondutor + "\nnome : " + nome 
                + "\nNumCartaConducao: " + NumCartaConducao + "\n";
    }

}
