/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.Cliente;
import isep.eapli.demo_orm.dominio.Condutor;
import isep.eapli.demo_orm.dominio.ContratoAluguer;
import isep.eapli.demo_orm.persistencia.ContratoAluguerJPAImpl;
import isep.eapli.demo_orm.persistencia.ContratoAluguerRepositorio;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Wimy Carvalho
 */
public class ContratoAluguerController {

    private ContratoAluguer contratoAluguer;

    public void registarContratoAluguer(Cliente Cliente, Automovel Automovel, Calendar DataIni, Calendar DataFim) {
        this.contratoAluguer = new ContratoAluguer(Cliente, Automovel, DataIni, DataFim);
        ContratoAluguerRepositorio repo = new ContratoAluguerJPAImpl();
        repo.add(this.contratoAluguer);
    }

    public List<ContratoAluguer> listarContratosAluguer() {
        ContratoAluguerJPAImpl repo = new ContratoAluguerJPAImpl();
        for (ContratoAluguer ca : repo.findAll()) {
            System.out.println(ca.toString());
        }
        return repo.findAll();
    }

    public ContratoAluguer procurarContratoAluguerID(long id) {
        ContratoAluguerJPAImpl repo = new ContratoAluguerJPAImpl();
        System.out.println(repo.findById(id).toString());
        return repo.findById(id);
    }
}
