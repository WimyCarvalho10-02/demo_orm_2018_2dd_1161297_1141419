/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;


import isep.eapli.demo_orm.dominio.ContratoAluguer;
import isep.eapli.demo_orm.dominio.Pagamento;
import isep.eapli.demo_orm.dominio.TipoPagamento;
import isep.eapli.demo_orm.persistencia.ContratoAluguerJPAImpl;
import isep.eapli.demo_orm.persistencia.PagamentoRepositorio;
import isep.eapli.demo_orm.persistencia.PagamentoRepositorioJPAImpl;
import java.util.List;

/**
 *
 * @author wimyd
 */
public class PagamentoController {

    public void registarPagamento(ContratoAluguer contrato, TipoPagamento tipo) {
        PagamentoRepositorio repo = new PagamentoRepositorioJPAImpl();
        repo.add(new Pagamento(tipo, contrato));
    }

    public List<Pagamento> listarPagamentos() {
        PagamentoRepositorio repo = new PagamentoRepositorioJPAImpl();
        for (Pagamento ca : repo.findAll()) {
            System.out.println(ca.toString());
        }
        return repo.findAll();
    }
}
