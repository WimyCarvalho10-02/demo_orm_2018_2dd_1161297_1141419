/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.GrupoAutomovelRepositorio;
import isep.eapli.demo_orm.persistencia.GrupoAutomovelRepositorioJPAImpl;
import java.util.List;


/**
 *
 * @author eapli
 */
public class GrupoAutomovelController {

    public GrupoAutomovel registarGrupoAutomóvel(String nome, int portas,
            String classe) {
        GrupoAutomovel grupo1 = new GrupoAutomovel(nome, portas, classe);
        GrupoAutomovelRepositorio repo = new GrupoAutomovelRepositorioJPAImpl();
        return repo.add(grupo1);
    }

    public List<GrupoAutomovel> listarGruposAutomoveis() {
        GrupoAutomovelRepositorioJPAImpl repo = new GrupoAutomovelRepositorioJPAImpl();
        for(GrupoAutomovel ga : repo.findAll()){
            System.out.println(ga.toString());
        }
        return repo.findAll();
    }

    public GrupoAutomovel procurarGrupoAutomovel(long id) {
        GrupoAutomovelRepositorioJPAImpl repo = new GrupoAutomovelRepositorioJPAImpl();
        System.out.println(repo.findById(id).toString());
        return repo.findById(id);
    }
}
