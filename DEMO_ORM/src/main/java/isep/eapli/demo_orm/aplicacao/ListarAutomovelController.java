/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.AutomovelRepositorioJPAImpl;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eduardo Gomes
 */
public class ListarAutomovelController {

    public List<Automovel> listarAutomoveis() {
        AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
        for (Automovel a : repo.findAll()) {
            System.out.println(a.toString());
        }
        return repo.findAll();
    }

    public Automovel procurarAutomovelMatricula(String Matricula) {
        AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
        List<Automovel> automovel = repo.findAll();
        for (Automovel a : automovel) {
            if (a.getMatricula().equalsIgnoreCase(Matricula)) {
                System.out.println(a);
                return a;
            }
        }
        return null;
    }

    public void procurarAutomovelPorGrupo(GrupoAutomovel Grupo) {
        AutomovelRepositorioJPAImpl repo = new AutomovelRepositorioJPAImpl();
        List<Automovel> automovelPorGrupo = repo.findAll();
        for (Automovel a : automovelPorGrupo) {
            if (a.getGrupo().equals(Grupo)) {
                System.out.println(a);
            }
        }
    }
}
