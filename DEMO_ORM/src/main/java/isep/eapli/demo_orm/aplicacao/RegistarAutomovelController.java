/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.persistencia.*;


/**
 *
 * @author Eduardo Gomes
 */
public class RegistarAutomovelController {

    public Automovel registarAutomovel(int Km, String Matricula, GrupoAutomovel Grupo) {
       Automovel a1 = new Automovel(Km, Matricula, Grupo);
        AutomovelRepositorio repo = new AutomovelRepositorioJPAImpl();
        return repo.add(a1);
    }
    
}
