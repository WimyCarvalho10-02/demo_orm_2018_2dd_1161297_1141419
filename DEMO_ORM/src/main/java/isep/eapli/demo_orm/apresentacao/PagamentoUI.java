/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.aplicacao.ContratoAluguerController;
import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.aplicacao.PagamentoController;
import isep.eapli.demo_orm.dominio.ContratoAluguer;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.dominio.TipoPagamento;

/**
 *
 * @author wimyd
 */
public class PagamentoUI {

    private final PagamentoController controller = new PagamentoController();
    private final ContratoAluguerController controller2 = new ContratoAluguerController();

    public void registarPA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        int ID = Console.readInteger("ID Contrato Aluguer:");
        int cod = Console.readInteger("**** Tipo de Pagamento ****\nCredito ->1 || Debito ->0 :");
        TipoPagamento t = null;
        if(cod == 1){
            t = t.Credito;
        }else{
            t= t.Debito;
        }
        ContratoAluguer contrato = controller2.procurarContratoAluguerID((long) ID);
        controller.registarPagamento(contrato, t);
    }

    public void listarPAs() {
        controller.listarPagamentos();
    }

    public void procurarPagamentoPorID(long id) {
        //controller.procurarGrupoAutomovel(id);
    }
}
