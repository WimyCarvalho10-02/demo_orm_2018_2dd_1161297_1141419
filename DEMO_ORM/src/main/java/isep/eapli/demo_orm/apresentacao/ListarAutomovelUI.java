/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.aplicacao.ListarAutomovelController;

/**
 *
 * @author Eduardo Gomes
 */
public class ListarAutomovelUI {
    
    private final ListarAutomovelController controller = new ListarAutomovelController();
    private final GrupoAutomovelController controller2 = new GrupoAutomovelController();
    
    public void listarAutomveis() {
        controller.listarAutomoveis();
    }
    
    public void procurarAutomobelPorMatricula(String Matricula) {
        controller.procurarAutomovelMatricula(Matricula);
    }
    
    public void procurarAutomovelPorGrupoAutomovel(Long grupo) {
        controller.procurarAutomovelPorGrupo(controller2.procurarGrupoAutomovel(grupo));
    }
}
