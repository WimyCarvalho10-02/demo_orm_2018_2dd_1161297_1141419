/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.aplicacao.RegistarAutomovelController;
import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.Scanner;

/**
 *
 * @author Eduardo Gomes
 */
public class RegistarAutomovelUI {
    
    private final RegistarAutomovelController controller = new RegistarAutomovelController();
    private final GrupoAutomovelController controller2 = new GrupoAutomovelController();
    
    public void registarAutomovel() {
        Scanner s = new Scanner(System.in);
        
        System.out.println("*** Registo Automóvel ***\n");
        int Km = Console.readInteger("Km:");
        String Matricula = Console.readLine("Matricula:");
        System.out.println("Insira o ID do Automóvel");
        long c = s.nextLong();
        GrupoAutomovel Grupo = controller2.procurarGrupoAutomovel(c);
        Automovel Automovel = controller.
                registarAutomovel(Km, Matricula, Grupo);
        
        System.out.println("Automóvel" + Automovel);
    }
}
