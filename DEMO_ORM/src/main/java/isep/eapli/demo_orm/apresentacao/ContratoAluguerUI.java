/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.aplicacao.ContratoAluguerController;
import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.aplicacao.ListarAutomovelController;
import isep.eapli.demo_orm.aplicacao.RegistarAutomovelController;
import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.Cliente;
import isep.eapli.demo_orm.dominio.Condutor;
import isep.eapli.demo_orm.dominio.ContratoAluguer;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Eduardo Gomes
 */
public class ContratoAluguerUI {

    private final ContratoAluguerController controller1 = new ContratoAluguerController();
    private final ListarAutomovelController controller2 = new ListarAutomovelController();

    public void registarContratoAluguer() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");

        System.out.println("*** Registo Contrato Aluguer ***\n");
        String NomeCliente = Console.readLine("Nome do Cliente: ");

        String matricula = Console.readLine("Matricula do Automovel");
        Automovel Automovel = controller2.procurarAutomovelMatricula(matricula);

        String DataIni = Console.readLine("Data Início Contrato: ");
        String DataFim = Console.readLine("Data Fim Contrato: ");
        Date dateIni = null;
        Date dateEnd = null;
        try {
            dateIni = sdf.parse(DataIni);
            dateEnd = sdf.parse(DataFim);

        } catch (Exception e) {
            e.getMessage();
        }
          
        Calendar cal = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        cal.setTime(dateIni);
        c2.setTime(dateEnd);
        
        ContratoAluguer c = new ContratoAluguer(new Cliente(NomeCliente), Automovel, cal, c2);
        int numCond = Console.readInteger("Número de Condutores a adicionar: ");
        for (int w = 0; w < numCond; w++) {
            String nome = Console.readLine("Insira o nome do Condutor:");
            String num = Console.readLine("Número da Carata:");
            c.addCondutorAutorizado(new Condutor(nome, num));
        }
        
        controller1.registarContratoAluguer(new Cliente(NomeCliente), Automovel, cal, c2);

    }

    public void listarContratosAluguer() {
        controller1.listarContratosAluguer();
    }

    public void procurarContratoAluguerID(long id) {
        controller1.procurarContratoAluguerID(id);
    }
}
