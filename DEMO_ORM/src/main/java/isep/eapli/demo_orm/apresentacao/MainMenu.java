/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import java.text.ParseException;
import java.util.Scanner;

/**
 *
 * @author mcn
 */
public class MainMenu {

    public static void mainLoop() {
        int opcao = 0;
        Scanner r = new Scanner(System.in);
        do {
            opcao = menu();

            switch (opcao) {
                case 0:
                    System.out.println("fim ...");
                    break;
                case 1:
                    new GrupoAutomovelUI().registarGA();
                    break;

                case 2:
                    new GrupoAutomovelUI().listarGAs();
                    break;
                case 3:
                    System.out.println("Lista de grupos automoveis disponíveis");
                    new GrupoAutomovelUI().listarGAs();
                    System.out.println("Insira o ID do grupo pretendido");
                    Long op = r.nextLong();
                    new GrupoAutomovelUI().procurarGAPorID(op);
                    break;
                case 4:
                    new RegistarAutomovelUI().registarAutomovel();
                    break;
                case 5:
                    new ListarAutomovelUI().listarAutomveis();
                    break;
                case 6:
                    System.out.println("Insira o ID do GrupoAutomovel");
                    Long Grupo = r.nextLong();
                    new ListarAutomovelUI().procurarAutomovelPorGrupoAutomovel(Grupo);
                    break;
                case 7:
                    System.out.println("Insira a matricula");
                    String matricula = r.nextLine();
                    new ListarAutomovelUI().procurarAutomobelPorMatricula(matricula);
                    break;
                case 8:
                    new ContratoAluguerUI().registarContratoAluguer();
                    break;
                case 9:
                    new ContratoAluguerUI().listarContratosAluguer();
                    break;
                case 10:
                    System.out.println("Insira o ID do Contrato Aluguer");
                    Long id = r.nextLong();
                    new ContratoAluguerUI().procurarContratoAluguerID(id);
                    break;
                case 11:
                    new PagamentoUI().registarPA();
                    break;
                case 12:
                    new PagamentoUI().listarPAs();
                    break;
                default:
                    System.out.println("opcao não reconhecida.");
                    break;
            }
        } while (opcao != 0);

    }

    private static int menu() {
        int option = -1;
        System.out.println("");
        System.out.println("=============================");
        System.out.println(" Rent a Car ");
        System.out.println("=============================\n");
        System.out.println("1.Registar Grupo Automóvel");
        System.out.println("2.Listar todos os Grupos Automóveis");
        System.out.println("3.Listar Grupo Automóveis por ID");
        System.out.println("4.Registar Automovel");
        System.out.println("5.Listar Automovel");
        System.out.println("6.Listar Automovel por determinado Grupo Automovel");
        System.out.println("7.Procurar Automovel por Matricula");
        System.out.println("8.Registar Novo Contrato de Aluguer");
        System.out.println("9.Listar todos os Contrato de Aluguer");
        System.out.println("10.Procurar Contrato de Aluguer por ID");
        System.out.println("11.Pagamento Contrato de Aluguer");
        System.out.println("12.Listar Pagamentos");
        System.out.println("=============================");
        System.out.println("0. Sair\n\n");
        option = Console.readInteger("Por favor escolha opção");
        return option;
    }
}
