/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.ContratoAluguer;
import java.util.List;

/**
 *
 * @author Eduardo Gomes
 */
public interface ContratoAluguerRepositorio {

    /**
     * inserts an entity and commits
     *
     * @param entity
     * @return the persisted entity
     */
    public ContratoAluguer add(ContratoAluguer entity);

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    public List<ContratoAluguer> findAll();

    /**
     * reads an entity given its ID
     *
     * @param id
     * @return
     */
    public ContratoAluguer findById(Long id);
}
