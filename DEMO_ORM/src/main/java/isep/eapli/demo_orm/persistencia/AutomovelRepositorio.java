/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;

/**
 *
 * @author Eduardo Gomes
 */
public interface AutomovelRepositorio {

    /**
     * inserts an entity and commits
     *
     * @param a1
     * @return the persisted entity
     */
    public Automovel add(Automovel a1);

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    public List<Automovel> findAll();

    /**
     * reads an entity given its Matricula
     *
     * @param Matricula
     * @return
     */
    public Automovel findByMatricula(String Matricula);

    /**
     * reads an entity given its Grupo
     *
     * @return
     */
    public Automovel findByGrupo(GrupoAutomovel GrupoAutomovel);
}
