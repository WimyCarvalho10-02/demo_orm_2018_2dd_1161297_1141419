/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.Automovel;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Eduardo Gomes
 */
public class AutomovelRepositorioJPAImpl implements AutomovelRepositorio {

    public AutomovelRepositorioJPAImpl() {
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("DEMO_ORM_PU");
        EntityManager manager = factory.createEntityManager();
        return manager;
    }

    /**
     * inserts an entity Automovel
     *
     * @param Automovel
     * @return the persisted entity
     */
    @Override
    public Automovel add(Automovel Automovel) {
        if (Automovel == null) {
            throw new IllegalArgumentException();
        }
        EntityManager em = getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(Automovel);
        tx.commit();
        em.close();

        return Automovel;
    }

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    //@SuppressWarnings("unchecked")
    @Override
    public List<Automovel> findAll() {
        Query query = getEntityManager().createQuery(
                "SELECT e FROM Automovel e");
        List<Automovel> list = query.getResultList();
        return list;
    }

    /**
     * reads an entity GrupoAutomovel given its Matricula
     *
     * @param Matricula
     * @return
     */
    @Override
    public Automovel findByMatricula(String Matricula) {
        return getEntityManager().find(Automovel.class, Matricula);
    }

    /**
     * reads an entity GrupoAutomovel given its Matricula
     *
     * @param GrupoAutomovel
     * @return
     */
    @Override
    public Automovel findByGrupo(GrupoAutomovel Grupo) {
        return getEntityManager().find(Automovel.class, Grupo);
    }
}
