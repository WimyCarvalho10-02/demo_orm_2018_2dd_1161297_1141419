/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.Pagamento;
import java.util.List;

/**
 *
 * @author wimyd
 */
public interface PagamentoRepositorio {
     /**
     * inserts an entity and commits
     *
     * @param a1
     * @return the persisted entity
     */
    public Pagamento add(Pagamento a1);

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    public List<Pagamento> findAll();

}
