/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.persistencia;

import isep.eapli.demo_orm.dominio.ContratoAluguer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Eduardo Gomes
 */
public class ContratoAluguerJPAImpl implements ContratoAluguerRepositorio {

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.
                createEntityManagerFactory("DEMO_ORM_PU");
        EntityManager manager = factory.createEntityManager();
        return manager;
    }

    /**
     * inserts an entity ContratoAluguer
     *
     * @param ContratoAluguer
     * @return the persisted entity
     */
    @Override
    public ContratoAluguer add(ContratoAluguer ContratoAluguer) {
        if (ContratoAluguer == null) {
            throw new IllegalArgumentException();
        }
        EntityManager em = getEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(ContratoAluguer);
        tx.commit();
        em.close();

        return ContratoAluguer;
    }

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    //@SuppressWarnings("unchecked")
    @Override
    public List<ContratoAluguer> findAll() {
        Query query = getEntityManager().createQuery(
                "SELECT e FROM ContratoAluguer e");
        List<ContratoAluguer> list = query.getResultList();
        return list;
    }

    /**
     * reads an entity ContratoAluguer given its ID
     *
     * @param id
     * @return
     */
    @Override
    public ContratoAluguer findById(Long id) {
        return getEntityManager().find(ContratoAluguer.class, id);
    }

}
