/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wimyd
 */
public class ClienteTest {
    
    /**
     * Test of toString method, of class Cliente.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cliente instance = new Cliente("Wimy");
        String s = "Cliente{IdCliente=0, nome=Wimy}";
        assertEquals(instance.toString(), s);
    }
    
    
    /**
     * Test of toString method, of class Cliente.
     */
    @Test
    public void testToString2() {
        System.out.println("toString");
        Cliente instance = new Cliente("Wimy");
        assertNotEquals(instance.toString() ,not(equalTo("S")));
    }
    
}
