/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author wimyd
 */
public class CondutorTest {

    /**
     * Test of toString method, of class Condutor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Condutor instance = new Condutor("C","12");
        Condutor i = new Condutor("C","12");
        assertEquals(instance.toString(), i.toString());
    }
    
    
    /**
     * Test of toString method, of class Condutor.
     */
    @Test
    public void testToString2() {
        System.out.println("toString");
        Condutor instance = new Condutor("C","12");
        Condutor i = new Condutor("D","12");
        assertNotEquals(instance.toString(), not(equalTo(i.toString())));
    }
}
