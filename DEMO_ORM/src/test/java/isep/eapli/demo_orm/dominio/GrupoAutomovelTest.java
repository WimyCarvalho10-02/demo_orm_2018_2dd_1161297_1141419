/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.xml.bind.ValidationException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author wimyd
 */
public class GrupoAutomovelTest {

    private GrupoAutomovel g;

    /**
     * Test of setNportas method, of class GrupoAutomovel.
     */
    @Test
    public void testSetNportas() {
        System.out.println("setNportas");
        int portas = 0;
        GrupoAutomovel instance = new GrupoAutomovel();
        instance.setNportas(portas);
        assertEquals(portas,instance.getNportas());
    }


    /**
     * Test of toString method, of class GrupoAutomovel.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        g = new GrupoAutomovel("23321", 3,"d");
        GrupoAutomovel instance = new GrupoAutomovel("23321", 3,"d");
        assertEquals(instance.toString(), g.toString());
    }

}
